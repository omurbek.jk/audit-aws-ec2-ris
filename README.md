audit EC2 RI
============================
This stack will monitor EC2 RI and alert on things CloudCoreo developers think are violations of best practices


## Description
This composite examines various EC2 Reserved Instance statistics and reports on these metrics and other utilization findings

## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/audit-aws-ec2-ris/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

**None**


## Required variables with default

### `AUDIT_AWS_EC2_RIS_ALLOW_EMPTY`:
  * description: Would you like to receive empty reports? Options - true / false. Default is false.
  * default: false

### `AUDIT_AWS_EC2_RIS_SEND_ON`:
  * description: Send reports always or only when there is a change? Options - always / change. Default is change.
  * default: change

### `AUDIT_AWS_EC2_RIS_REGIONS`:
  * description: List of AWS regions to check. Default is all regions. Choices are us-east-1,us-east-2,us-west-1,us-west-2,ca-central-1,ap-south-1,ap-northeast-2,ap-southeast-1,ap-southeast-2,ap-northeast-1,eu-central-1,eu-west-1,eu-west-1,sa-east-1
  * default: us-east-1, us-east-2, us-west-1, us-west-2, ca-central-1, ap-south-1, ap-northeast-2, ap-southeast-1, ap-southeast-2, ap-northeast-1, eu-central-1, eu-west-1, eu-west-2, sa-east-1


## Optional variables with default

### `AUDIT_AWS_EC2_RIS_ALERT_LIST`:
  * description: Which alerts would you like to check for? Default is all ec2 ris alerts. Possible value is ec2-get-all-instances-older-than
  * default: ec2-unused-ris, ec2-instances-no-ri, ec2-covered-by-ris, ec2-percent-used-ris, ri-inventory, spot-inventory, spot-inventory-undisplayed, ec2-inventory

### `AUDIT_AWS_EC2_RIS_OWNER_TAG`:
  * description: Enter an AWS tag whose value is an email address of the owner of the EC2 object. (Optional)
  * default: NOT_A_TAG


## Optional variables with no default

### `HTML_REPORT_SUBJECT`:
  * description: Enter a custom report subject name.

### `AUDIT_AWS_EC2_RIS_RECIPIENT`:
  * description: Enter the email address(es) that will receive notifications. If more than one, separate each with a comma.

### `AUDIT_AWS_EC2_RIS_S3_NOTIFICATION_BUCKET_NAME`:
  * description: Enter S3 bucket name to upload reports. (Optional)

## Tags
1. Operations
1. EC2 Reserved Instances

## Categories
1. AWS Operations Automation



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/audit-aws-ec2-ris/master/images/diagram.png "diagram")


## Icon
![icon](https://raw.githubusercontent.com/CloudCoreo/audit-aws-ec2-ris/master/images/icon.png "icon")


