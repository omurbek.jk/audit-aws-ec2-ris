coreo_aws_rule "ec2-unused-ris" do
  action :define
  service :user
  display_name "Reserved instances purchased and unused"
  description "This rule checks for any active and purchased reserved instances that are not covering any RIs"
  category "Spend"
  suggested_action "Consider launching instances with RI coverage"
  level "Inventory"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
end

coreo_aws_rule "ec2-instances-no-ri" do
  action :define
  service :user
  display_name "Instance is running w/out RI coverage"
  description "This rule will determine what instances are not covered by an RI "
  category "Spend"
  suggested_action "None"
  level "Inventory"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
end

coreo_aws_rule "ec2-covered-by-ris" do
  action :define
  service :user
  display_name "Instance has RI coverage"
  description "This rule will determine what instances are covered by an RI "
  category "Spend"
  suggested_action "None"
  level "Inventory"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
end

coreo_aws_rule "ec2-percent-used-ris" do
  action :define
  service :user
  display_name "The percentage of your purchased RI's that are being used"
  description "This rule determines the percentage of RI's you are utilizing"
  category "Spend"
  suggested_action "None"
  level "Inventory"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
end

coreo_aws_rule "ri-inventory" do
  action :define
  service :ec2
  objectives ["reserved_instances"]
  audit_objects ["object.reserved_instances.state"]
  operators ["=="]
  raise_when ["active"]
  id_map "object.reserved_instances.reserved_instances_id"
end

coreo_aws_rule "spot-inventory" do
  action :define
  service :user
  display_name "Spot Instances"
  description "Spot Instances"
  category "Spend"
  suggested_action "None"
  level "Inventory"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
end

coreo_aws_rule "spot-inventory-undisplayed" do
  action :define
  service :ec2
  link ""
  display_name "Inventory of Spot Instances"
  description "An inventory of Spot Instances"
  category "Inventory"
  suggested_action "N/A"
  level "Informational"
  objectives ["instances", "instances"]
  audit_objects ["object.reservations.instances.state.name", "object.reservations.instances.instance_lifecycle"]
  operators ["==", "=="]
  raise_when ["running", "spot"]
  id_map "object.reservations.instances.instance_id"
end

coreo_aws_rule "ec2-inventory" do
  action :define
  service :ec2
  link ""
  display_name "Inventory of EC2 (non-spot) Instances"
  description "An inventory of EC2 (non-spot) Instances"
  category "Inventory"
  suggested_action "N/A"
  level "Informational"
  objectives ["instances"]
  audit_objects ["object.reservations.instances.state.name"]
  operators ["=="]
  raise_when ["running"]
  id_map "object.reservations.instances.instance_id"
end


coreo_uni_util_variables "ec2-ris-planwide" do
  action :set
  variables([
                {'COMPOSITE::coreo_uni_util_variables.ec2-ris-planwide.composite_name' => 'PLAN::stack_name'},
                {'COMPOSITE::coreo_uni_util_variables.ec2-ris-planwide.plan_name' => 'PLAN::name'},
                {'COMPOSITE::coreo_uni_util_variables.ec2-ris-planwide.results' => 'unset'},
                {'GLOBAL::number_violations' => '0'}
            ])
end

coreo_aws_rule_runner "advise-ec2-ris" do
  action :run
  service :ec2
  rules ${AUDIT_AWS_EC2_RIS_ALERT_LIST}
end

coreo_uni_util_variables "ec2-ris-update-planwide-1" do
  action :set
  variables([
                {'COMPOSITE::coreo_uni_util_variables.ec2-ris-planwide.results' => 'COMPOSITE::coreo_aws_rule_runner_ec2-ris.advise-ec2-ris.report'},
                {'GLOBAL::number_violations' => 'COMPOSITE::coreo_aws_rule_runner_ec2-ris.advise-ec2-ris.number_violations'},

            ])
end

coreo_uni_util_jsrunner "advise-ec2-ris" do
  action :run
  json_input 'COMPOSITE::coreo_aws_rule_runner.advise-ec2-ris.report'
  function <<-EOH
    var ris = {};
    var onDemands = {};
    var spots = {};

    var totalNumberOfRIs = 0;
    var totalNumberOfInstances = 0;

    for (var region in json_input) {
        if (json_input.hasOwnProperty(region)) {
            var outerViolators = json_input[region];
            for(var violator in outerViolators){
                if(outerViolators.hasOwnProperty(violator)){
                    var instanceType = outerViolators[violator]['violator_info']['instance_type'];
                    if(outerViolators[violator]['violations'].hasOwnProperty('ri-inventory')){
                        if(ris[region] === undefined){ris[region] = {}};
                        if(ris[region][instanceType] === undefined){ris[region][instanceType] = {}};
                        if(ris[region][instanceType][violator] === undefined){ris[region][instanceType][violator] = {}};
                        totalNumberOfRIs += 1;
                        ris[region][instanceType][violator] = outerViolators[violator]['violator_info'];
                    } else {
                        if(onDemands[region] === undefined){onDemands[region] = {}};
                        if(onDemands[region][instanceType] === undefined){onDemands[region][instanceType] = {}};
                        if(onDemands[region][instanceType][violator] === undefined){onDemands[region][instanceType][violator] = {}};
                        totalNumberOfInstances += 1;
                        onDemands[region][instanceType][violator] = outerViolators[violator]['violator_info'];
                    }
                    // set up the spot instances
                    if(outerViolators[violator]['violations'].hasOwnProperty('spot-inventory-undisplayed')){
                        if(spots[region] === undefined){spots[region] = {}};
                        if(spots[region][instanceType] === undefined){spots[region][instanceType] = {}};
                        if(spots[region][instanceType][violator] === undefined){spots[region][instanceType][violator] = {}};
                        totalNumberOfInstances += 1;
                        spots[region][instanceType][violator] = outerViolators[violator]['violator_info'];
                        // remove all spot instances from the onDemands
                        delete onDemands[region][instanceType][violator];
                    }
                }
            }
        }
    }

    var unusedRIs = JSON.parse(JSON.stringify(ris));
    var unusedInstances = JSON.parse(JSON.stringify(onDemands));
    var coveredInstances = {};
    var allSpots = JSON.parse(JSON.stringify(spots));

    var usedRis = 0;

    const unusedRisMetadata = {
        'service': 'ec2',
        'display_name': 'Unused RI instances',
        'description': 'Checks for unused RIs',
        'category': 'Spend',
        'suggested_action': 'Consider launching instances that are covered by the RIs',
        'level': 'Informational'
    };

    const coveredByRIsMetadata = {
        'service': 'ec2',
        'display_name': 'EC2 instances covered by RIs',
        'description': 'EC2 instances covered by RIs',
        'category': 'Spend',
        'suggested_action': 'These instances are running under RIs',
        'level': 'Informational'
    };

    const unCoveredInstancesMetadata = {
        'service': 'ec2',
        'display_name': 'Running instances not covered by an RI',
        'description': 'Checks for Instances not covered by an RI',
        'category': 'Spend',
        'suggested_action': 'Consider launching instances that are covered by the RIs',
        'level': 'Informational'
    };

    const spotInventoryMetadata = {
        'service': 'ec2',
        'display_name': 'Inventory of Spot Instances',
        'description': 'An inventory of Spot Instances',
        'category': 'Spend',
        'suggested_action': 'N/A',
        'level': 'Informational'
    };

    var unusedRIReport = {};
    for(var region in ris){
        for(var riInstanceType in ris[region]) {
            for(var riId in ris[region][riInstanceType]){
                //check if there is an instance covering this instance type
                if(onDemands[region] && onDemands[region][riInstanceType]){

                    if(unusedRIReport === undefined){unusedRIReport = {}};
                    if(unusedRIReport[region] === undefined){unusedRIReport[region] = {}};
                    if(unusedRIReport[region][riId] === undefined){unusedRIReport[region][riId] = {}};
                    if(unusedRIReport[region][riId]['violations'] === undefined){unusedRIReport[region][riId]['violations'] = {}};
                    unusedRIReport[region][riId]['violations']['ec2-covered-by-ris'] = coveredByRIsMetadata
                    unusedRIReport[region][riId]['violator_info'] = unusedInstances[region][riInstanceType][Object.keys(unusedInstances[region][riInstanceType])[0]];

                    // delete one of the ri's now
                    delete unusedRIs[region][riInstanceType][Object.keys(unusedRIs[region][riInstanceType])[0]];
                    delete unusedInstances[region][riInstanceType][Object.keys(unusedInstances[region][riInstanceType])[0]];
                    usedRis += 1;
                }
            }
        }
    }

    // build the report now
    for(var region in unusedRIs){
        for(var riInstanceType in unusedRIs[region]) {
            for(var riId in unusedRIs[region][riInstanceType]){
                //check if there is an instance covering this instance type
                if(unusedRIReport === undefined){unusedRIReport = {}};
                if(unusedRIReport[region] === undefined){unusedRIReport[region] = {}};
                if(unusedRIReport[region][riId] === undefined){unusedRIReport[region][riId] = {}};
                if(unusedRIReport[region][riId]['violations'] === undefined){unusedRIReport[region][riId]['violations'] = {}};

                unusedRIReport[region][riId]['violations']['ec2-unused-ris'] = unusedRisMetadata
                unusedRIReport[region][riId]['violator_info'] = unusedRIs[region][riInstanceType][riId];
            }
        }
    }
    for(var region in allSpots){
        for(var spotInstanceType in allSpots[region]) {
            for(var spotId in allSpots[region][spotInstanceType]){
                //check if there is an instance covering this instance type
                if(unusedRIReport === undefined){unusedRIReport = {}};
                if(unusedRIReport[region] === undefined){unusedRIReport[region] = {}};
                if(unusedRIReport[region][spotId] === undefined){unusedRIReport[region][spotId] = {}};
                if(unusedRIReport[region][spotId]['violations'] === undefined){unusedRIReport[region][spotId]['violations'] = {}};

                unusedRIReport[region][spotId]['violations']['spot-inventory'] = spotInventoryMetadata
                unusedRIReport[region][spotId]['violator_info'] = allSpots[region][spotInstanceType][spotId];
            }
        }
    }
    for(var region in unusedInstances){
        for(var riInstanceType in unusedInstances[region]) {
            for(var riId in unusedInstances[region][riInstanceType]){
                //check if there is an instance covering this instance type
                if(unusedRIReport === undefined){unusedRIReport = {}};
                if(unusedRIReport[region] === undefined){unusedRIReport[region] = {}};
                if(unusedRIReport[region][riId] === undefined){unusedRIReport[region][riId] = {}};
                if(unusedRIReport[region][riId]['violations'] === undefined){unusedRIReport[region][riId]['violations'] = {}};

                unusedRIReport[region][riId]['violations']['ec2-instances-no-ri'] = unCoveredInstancesMetadata;
                unusedRIReport[region][riId]['violator_info'] = unusedInstances[region][riInstanceType][riId];
            }
        }
    }
    var percentRisUsed = 100;
    if(totalNumberOfRIs > 0) {
        percentRisUsed = 100 * (usedRis / totalNumberOfRIs);
    }

    var percentLevel = 'Critical';
    if(percentRisUsed > 33) { percentLevel = 'Alert'};
    if(percentRisUsed > 66) { percentLevel = 'Warning'};
    if(percentRisUsed == 100) { percentLevel = 'Informational'};

    const percentRisUsedMetadata = {
        'service': 'ec2',
        'display_name': `Current utilization - ${percentRisUsed}%`,
        'description': 'The percentage of your RIs being utilized'  ,
        'category': 'Spend',
        'suggested_action': 'Consider launching instances that are covered by the RIs',
        'level': `${percentLevel}`
    };

    var percentRIReport = {};
    if(unusedRIReport['us-east-1'] === undefined){unusedRIReport['us-east-1'] = {}};
    if(unusedRIReport['us-east-1']['ec2-percent-used-ris'] === undefined){unusedRIReport['us-east-1']['ec2-percent-used-ris'] = {}};
    if(unusedRIReport['us-east-1']['ec2-percent-used-ris']['violations'] === undefined){unusedRIReport['us-east-1']['ec2-percent-used-ris']['violations'] = {}};
    unusedRIReport['us-east-1']['ec2-percent-used-ris']['violations']['ec2-percent-used-ris'] = percentRisUsedMetadata;
    unusedRIReport['us-east-1']['ec2-percent-used-ris']['violator_info'] = {'percent_used': percentRisUsed};

    // build the report now
    for(var region in unusedRIs){
        for(var riInstanceType in unusedRIs[region]) {
            for(var riId in unusedRIs[region][riInstanceType]){
                //check if there is an instance covering this instance type
                if(unusedRIReport === undefined){unusedRIReport = {}};
                if(unusedRIReport[region] === undefined){unusedRIReport[region] = {}};
                if(unusedRIReport[region][riId] === undefined){unusedRIReport[region][riId] = {}};
                if(unusedRIReport[region][riId]['violations'] === undefined){unusedRIReport[region][riId]['violations'] = {}};

                unusedRIReport[region][riId]['violations']['ec2-unused-ris'] = unusedRisMetadata
                unusedRIReport[region][riId]['violator_info'] = unusedRIs[region][riInstanceType][riId];
            }
        }
    }

    coreoExport('usedRis', usedRis);
    coreoExport('totalNumberOfRIs', totalNumberOfRIs);
    coreoExport('totalNumberOfInstances', totalNumberOfInstances);
    coreoExport('percentRisUsed', percentRisUsed);
    coreoExport('report', JSON.stringify(unusedRIReport));

    callback(unusedRIReport);

  EOH
end

coreo_uni_util_variables "ec2-ris-update-planwide-3" do
  action :set
  variables([
                {'COMPOSITE::coreo_aws_rule_runner.advise-ec2-ris.report' => 'COMPOSITE::coreo_uni_util_jsrunner.advise-ec2-ris.report'},
            ])
end

coreo_uni_util_jsrunner "ec2-ris-tags-to-notifiers-array" do
  action :run
  data_type "json"
  provide_composite_access true
  packages([
               {
                   :name => "cloudcoreo-jsrunner-commons",
                   :version => "1.10.7-beta64"
               },
               {
                   :name => "js-yaml",
                   :version => "3.7.0"
               }       ])
  json_input '{ "compositeName":"PLAN::stack_name",
                "planName":"PLAN::name",
                "teamName":"PLAN::team_name",
                "cloudAccountName": "PLAN::cloud_account_name",
                "violations": COMPOSITE::coreo_aws_rule_runner.advise-ec2-ris.report}'
  function <<-EOH

const compositeName = json_input.compositeName;
const planName = json_input.planName;
const cloudAccount = json_input.cloudAccountName;
const cloudObjects = json_input.violations;
const teamName = json_input.teamName;

const NO_OWNER_EMAIL = "${AUDIT_AWS_EC2_RIS_RECIPIENT}";
const OWNER_TAG = "${AUDIT_AWS_EC2_RIS_OWNER_TAG}";
const ALLOW_EMPTY = "${AUDIT_AWS_EC2_RIS_ALLOW_EMPTY}";
const SEND_ON = "${AUDIT_AWS_EC2_RIS_SEND_ON}";
const htmlReportSubject = "${HTML_REPORT_SUBJECT}";

const alertListArray = ${AUDIT_AWS_EC2_RIS_ALERT_LIST};
const ruleInputs = {};

let userSuppression;
let userSchemes;

const fs = require('fs');
const yaml = require('js-yaml');
function setSuppression() {
  try {
      userSuppression = yaml.safeLoad(fs.readFileSync('./suppression.yaml', 'utf8'));
  } catch (e) {
    if (e.name==="YAMLException") {
      throw new Error("Syntax error in suppression.yaml file. "+ e.message);
    }
    else{
      console.log(e.name);
      console.log(e.message);
      userSuppression=[];
    }
  }

  coreoExport('suppression', JSON.stringify(userSuppression));
}

function setTable() {
  try {
    userSchemes = yaml.safeLoad(fs.readFileSync('./table.yaml', 'utf8'));
  } catch (e) {
    if (e.name==="YAMLException") {
      throw new Error("Syntax error in table.yaml file. "+ e.message);
    }
    else{
      console.log(e.name);
      console.log(e.message);
      userSchemes={};
    }
  }

  coreoExport('table', JSON.stringify(userSchemes));
}
setSuppression();
setTable();

const argForConfig = {
    NO_OWNER_EMAIL, cloudObjects, userSuppression, OWNER_TAG,
    userSchemes, alertListArray, ruleInputs, ALLOW_EMPTY,
    SEND_ON, cloudAccount, compositeName, planName, htmlReportSubject, teamName
}


function createConfig(argForConfig) {
    let JSON_INPUT = {
        compositeName: argForConfig.compositeName,
        htmlReportSubject: argForConfig.htmlReportSubject,
        planName: argForConfig.planName,
        teamName: argForConfig.teamName,
        teamName: argForConfig.teamName,
        violations: argForConfig.cloudObjects,
        userSchemes: argForConfig.userSchemes,
        userSuppression: argForConfig.userSuppression,
        alertList: argForConfig.alertListArray,
        disabled: argForConfig.ruleInputs,
        cloudAccount: argForConfig.cloudAccount
    };
    let SETTINGS = {
        NO_OWNER_EMAIL: argForConfig.NO_OWNER_EMAIL,
        OWNER_TAG: argForConfig.OWNER_TAG,
        ALLOW_EMPTY: argForConfig.ALLOW_EMPTY, SEND_ON: argForConfig.SEND_ON,
        SHOWN_NOT_SORTED_VIOLATIONS_COUNTER: false
    };
    return {JSON_INPUT, SETTINGS};
}

const {JSON_INPUT, SETTINGS} = createConfig(argForConfig);
const CloudCoreoJSRunner = require('cloudcoreo-jsrunner-commons');

const emails = CloudCoreoJSRunner.createEmails(JSON_INPUT, SETTINGS);
const suppressionJSON = CloudCoreoJSRunner.createJSONWithSuppress(JSON_INPUT, SETTINGS);

coreoExport('JSONReport', JSON.stringify(suppressionJSON));
coreoExport('report', JSON.stringify(suppressionJSON['violations']));

callback(emails);
  EOH
end

coreo_uni_util_variables "ec2-ris-update-planwide-3" do
  action :set
  variables([
                {'COMPOSITE::coreo_uni_util_variables.ec2-planwide.results' => 'COMPOSITE::coreo_uni_util_jsrunner.ec2-ris-tags-to-notifiers-array.JSONReport'},
                {'COMPOSITE::coreo_aws_rule_runner.advise-ec2-ris.report' => 'COMPOSITE::coreo_uni_util_jsrunner.ec2-ris-tags-to-notifiers-array.report'},
                {'GLOBAL::table' => 'COMPOSITE::coreo_uni_util_jsrunner.ec2-ris-tags-to-notifiers-array.table'}
            ])
end

coreo_uni_util_jsrunner "ec2-ris-tags-rollup" do
  action :run
  data_type "text"
  json_input 'COMPOSITE::coreo_uni_util_jsrunner.ec2-ris-tags-to-notifiers-array.return'
  function <<-EOH
const notifiers = json_input;

function setTextRollup() {
    let emailText = '';
    let numberOfViolations = 0;
    let usedEmails=new Map();
    notifiers.forEach(notifier => {
        const hasEmail = notifier['endpoint']['to'].length;
        const email = notifier['endpoint']['to'];
        if(hasEmail && usedEmails.get(email)!==true) {
            usedEmails.set(email,true);
            numberOfViolations += parseInt(notifier['num_violations']);
            emailText += "recipient: " + notifier['endpoint']['to'] + " - " + "Violations: " + notifier['numberOfViolatingCloudObjects'] + ", Cloud Objects: "+ (notifier["num_violations"]-notifier['numberOfViolatingCloudObjects']) + "\\n";
        }
    });

    textRollup += 'Total Number of matching Cloud Objects: ' + numberOfViolations + "\\n";
    textRollup += 'Rollup' + "\\n";
    textRollup += emailText;

}


let textRollup = '';
setTextRollup();
callback(textRollup);
  EOH
end

coreo_uni_util_notify "advise-ec2-ris-to-tag-values" do
  action((("${AUDIT_AWS_EC2_RIS_RECIPIENT}".length > 0)) ? :notify : :nothing)
  notifiers 'COMPOSITE::coreo_uni_util_jsrunner.ec2-ris-tags-to-notifiers-array.return'
end

coreo_uni_util_notify "advise-ec2-rollup" do
  action((("${AUDIT_AWS_EC2_RIS_RECIPIENT}".length > 0) and (! "${AUDIT_AWS_EC2_RIS_OWNER_TAG}".eql?("NOT_A_TAG"))) ? :notify : :nothing)
  type 'email'
  allow_empty ${AUDIT_AWS_EC2_RIS_ALLOW_EMPTY}
  send_on "${AUDIT_AWS_EC2_RIS_SEND_ON}"
  payload '
composite name: PLAN::stack_name
plan name: PLAN::name
COMPOSITE::coreo_uni_util_jsrunner.ec2-ris-tags-rollup.return
  '
  payload_type 'text'
  endpoint ({
      :to => '${AUDIT_AWS_EC2_RIS_RECIPIENT}', :subject => 'CloudCoreo ec2 rule results on PLAN::stack_name :: PLAN::name'
  })
end

coreo_aws_s3_policy "cloudcoreo-audit-aws-ec2-ris-policy" do
  action((("${AUDIT_AWS_EC2_RIS_S3_NOTIFICATION_BUCKET_NAME}".length > 0) ) ? :create : :nothing)
  policy_document <<-EOF
{
"Version": "2012-10-17",
"Statement": [
{
"Sid": "",
"Effect": "Allow",
"Principal":
{ "AWS": "*" }
,
"Action": "s3:*",
"Resource": [
"arn:aws:s3:::${AUDIT_AWS_EC2_RIS_S3_NOTIFICATION_BUCKET_NAME}/*",
"arn:aws:s3:::${AUDIT_AWS_EC2_RIS_S3_NOTIFICATION_BUCKET_NAME}"
]
}
]
}
  EOF
end

coreo_aws_s3_bucket "bucket-${AUDIT_AWS_EC2_RIS_S3_NOTIFICATION_BUCKET_NAME}" do
  action((("${AUDIT_AWS_EC2_RIS_S3_NOTIFICATION_BUCKET_NAME}".length > 0) ) ? :create : :nothing)
  bucket_policies ["cloudcoreo-audit-aws-ec2-ris-policy"]
end

coreo_uni_util_notify "cloudcoreo-audit-aws-ec2-ris-s3" do
  action((("${AUDIT_AWS_EC2_RIS_S3_NOTIFICATION_BUCKET_NAME}".length > 0) ) ? :notify : :nothing)
  type 's3'
  allow_empty true
  payload 'COMPOSITE::coreo_uni_util_jsrunner.ec2-ris-tags-to-notifiers-array.report'
  endpoint ({
      object_name: 'aws-ec2-ris-json',
      bucket_name: '${AUDIT_AWS_EC2_RIS_S3_NOTIFICATION_BUCKET_NAME}',
      folder: 'ec2-ris/PLAN::name',
      properties: {}
  })
end
